package rpc

import (
	"strings"
	"time"

	"github.com/rcrowley/go-metrics"
	"github.com/smallnest/rpcx/server"
	"github.com/smallnest/rpcx/serverplugin"
	"pkg.cocoad.mobi/x/zlog"
)

type Service struct {
	ServiceAddress string
	Etcd           []string
	BasePath       string
}

func NewServer(name string, service *Service, app interface{}) *server.Server {
	s := server.NewServer()
	addRegistryPlugin(s, service.ServiceAddress, service.BasePath, service.Etcd)
	log.Debug("add registry plugin", log.F{
		"name":      name,
		"address":   service.ServiceAddress,
		"base_path": service.BasePath,
		"servers":   service.Etcd,
	})
	s.RegisterName(strings.ToLower(name), app, "")

	return s
}

func addRegistryPlugin(s *server.Server, addr, basePath string, servers []string) {
	r := &serverplugin.EtcdRegisterPlugin{
		ServiceAddress: "tcp@" + addr,
		EtcdServers:    servers,
		BasePath:       basePath,
		Metrics:        metrics.NewRegistry(),
		UpdateInterval: time.Minute,
	}
	err := r.Start()
	if err != nil {
		log.Fatal("start etcd register fail", log.F{"error": err})
	}

	s.Plugins.Add(r)
}
