package rpc

import (
	"github.com/docker/libkv/store"
	"github.com/smallnest/rpcx/client"
	"strings"
	"time"
)

func NewClient(base, service string, servers []string) client.XClient {
	s := strings.ToLower(service)
	d := client.NewEtcdDiscovery(base, s, servers, &store.Config{
		ConnectionTimeout: 10 * time.Second,
	})

	return client.NewXClient(service, client.Failover, client.RandomSelect, d, client.DefaultOption)
}
